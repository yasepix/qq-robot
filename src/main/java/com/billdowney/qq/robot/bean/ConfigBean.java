package com.billdowney.qq.robot.bean;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import kotlin.io.FilesKt;
import lombok.Getter;
import lombok.Setter;
import net.mamoe.mirai.utils.BotConfiguration;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 项目配置类
 *
 * @author BillDowney
 * @date 2021/4/7 20:29
 */
@Component
@ConfigurationProperties(value = "project.qq-robot")
@Getter
public class ConfigBean implements ApplicationRunner {
    /**
     * 应用是否启动完成
     */
    private transient boolean isStarted = false;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.isStarted = true;
    }

    /**
     * 机器人启动标记
     */
    private boolean enable = true;
    /**
     * 命令匹配符
     */
    private String cmdChar = "#";
    /**
     * 机器人的工作空间
     */
    private String workspace = "cache";
    /**
     * 是否输出机器人日志
     */
    private boolean logOut = true;
    /**
     * root管理员qq列表
     */
    @Setter
    private Set<Long> rootManageQq = new HashSet<>();
    /**
     * 普通管理员qq列表
     */
    @Setter
    private Set<Long> normalManageQq = new HashSet<>();
    /**
     * 群管理权限是否有管理机器人的权限
     */
    private boolean groupAdminPermission = true;
    /**
     * #群白名单，强制过滤
     */
    @Setter
    private Set<Long> groupWhiteList = new HashSet<>();
    /**
     * alapi-token认证令牌
     */
    private String alapiToken = "";
    /**
     * kate-api-token认证令牌
     */
    private String kateApiToken = "";
    /**
     * qq群启用插件标记
     */
    private Map<Long, Map<String, Boolean>> pluginsStartFlag = new HashMap<>();

    public void setEnable(boolean enable) {
        this.enable = enable;
        this.writeFile();
    }

    public void setCmdChar(String cmdChar) {
        this.cmdChar = cmdChar;
        this.writeFile();
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
        this.writeFile();
    }

    public void setLogOut(boolean logOut) {
        this.logOut = logOut;
        this.writeFile();
    }

    public void addNormalManageQq(Long qq) {
        this.normalManageQq.add(qq);
        this.writeFile();
    }

    public void removeNormalManageQq(Long qq) {
        this.normalManageQq.remove(qq);
        this.writeFile();
    }

    public void setGroupAdminPermission(boolean groupAdminPermission) {
        this.groupAdminPermission = groupAdminPermission;
        this.writeFile();
    }

    public void addGroupWhiteList(Long qq) {
        this.groupWhiteList.add(qq);
        this.writeFile();
    }

    public void setAlapiToken(String alapiToken) {
        this.alapiToken = alapiToken;
        this.writeFile();
    }

    public void setKateApiToken(String kateApiToken) {
        this.kateApiToken = kateApiToken;
        this.writeFile();
    }

    public void setPluginsStartFlag(Map<Long, Map<String, Boolean>> pluginsStartFlag) {
        this.pluginsStartFlag = pluginsStartFlag;
        this.writeFile();
    }

    /**
     * 将配置写入工作目录
     */
    public void writeFile() {
        if (this.isStarted) {
            // 配置文件存放路径
            File configFile = FilesKt.resolve(BotConfiguration.getDefault().getWorkingDir(), this.getWorkspace() + File.separator + "config.json");
            JSONObject jsonObject = JSONUtil.parseObj(this);
            FileUtil.writeString(jsonObject.toStringPretty(), configFile, StandardCharsets.UTF_8);
        }
    }

    /**
     * 读取json文件到内存中
     */
    public void readFile() {
        if (this.isStarted) {
            // 配置文件存放路径
            File configFile = FilesKt.resolve(BotConfiguration.getDefault().getWorkingDir(), this.getWorkspace() + File.separator + "config.json");
            if (configFile.exists()) {
                ConfigBean configBean = JSONUtil.readJSONObject(configFile, StandardCharsets.UTF_8).toBean(ConfigBean.class);
                BeanUtil.copyProperties(configBean, this);
            }
        }
    }
}
