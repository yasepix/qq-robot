package com.billdowney.qq.robot.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * 返回到前台的数据实体
 *
 * @author BillDowney
 * @date 2021/3/31 21:55
 */
@Data
@ApiModel(value = "接口返回对象", description = "接口返回对象")
public class ResultBean<T> implements Serializable {
    public static final String MSG_SUCCESS = "成功";
    public static final String MSG_FAIL = "失败";

    @ApiModelProperty(value = "返回代码", example = "200", notes = "HttpStatus状态码")
    private Integer code;
    @ApiModelProperty(value = "成功标志")
    private boolean success;
    @ApiModelProperty(value = "返回处理消息")
    private String msg;
    @ApiModelProperty(value = "时间戳", notes = "返回数据的时间戳")
    private Long timestamp;
    @ApiModelProperty(value = "返回数据对象")
    private T data;

    public ResultBean() {
        this.timestamp = System.currentTimeMillis();
    }

    public static <T> ResultBean<T> success() {
        return success(null);
    }

    public static <T> ResultBean<T> success(T data) {
        return success(data, MSG_SUCCESS);
    }

    public static <T> ResultBean<T> success(T data, String msg) {
        return success(data, msg, HttpStatus.OK);
    }

    public static <T> ResultBean<T> success(T data, String msg, HttpStatus httpStatus) {
        ResultBean<T> result = new ResultBean<>();
        result.setData(data);
        result.setMsg(msg);
        result.setCode(httpStatus.value());
        return result;
    }

    public static <T> ResultBean<T> error() {
        return error(null);
    }

    public static <T> ResultBean<T> error(String msg) {
        return error(null, msg);
    }

    public static <T> ResultBean<T> error(T data) {
        return error(data, MSG_FAIL);
    }

    public static <T> ResultBean<T> error(T data, String msg) {
        return error(data, msg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static <T> ResultBean<T> error(T data, String msg, HttpStatus httpStatus) {
        ResultBean<T> result = new ResultBean<>();
        result.setData(data);
        result.setMsg(msg);
        result.setCode(httpStatus.value());
        return result;
    }

}
