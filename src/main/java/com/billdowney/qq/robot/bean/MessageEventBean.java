package com.billdowney.qq.robot.bean;

import com.billdowney.qq.robot.enums.RobotEventEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.mamoe.mirai.event.Event;

/**
 * 消息事件实体，用于消息事件传递
 *
 * @author BillDowney
 * @date 2021/3/31 21:33
 */
@Data
@ApiModel(value = "消息传送实体")
public class MessageEventBean {

    @ApiModelProperty(value = "机器人事件")
    private RobotEventEnum robotEventEnum;
    @ApiModelProperty(value = "消息事件", notes = "https://github.com/mamoe/mirai/blob/dev/mirai-core-api/src/commonMain/kotlin/event/events/README.md")
    private Event event;

}
