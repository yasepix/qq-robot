package com.billdowney.qq.robot.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import net.mamoe.mirai.Bot;

/**
 * 机器人实体信息
 *
 * @author BillDowney
 * @date 2021/3/31 23:10
 */
@Data
@ApiModel(value = "机器人信息")
public class BotBean {

    @ApiModelProperty(value = "qq")
    private Long qq;
    @ApiModelProperty(value = "昵称")
    private String nick;
    @ApiModelProperty(value = "在线状态")
    private boolean online;

    public BotBean() {
    }

    public BotBean(Bot bot) {
        this.qq = bot.getId();
        this.nick = bot.getNick();
        this.online = bot.isOnline();
    }

}
