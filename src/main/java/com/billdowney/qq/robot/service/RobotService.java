package com.billdowney.qq.robot.service;

import com.billdowney.qq.robot.bean.ConfigBean;
import com.billdowney.qq.robot.event.EventListeningHandle;
import com.billdowney.qq.robot.util.DeviceUtil;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.BotFactory;
import net.mamoe.mirai.utils.BotConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * 机器人服务类
 *
 * @author BillDowney
 * @date 2021/3/31 22:45
 */
@Service
@Slf4j
public class RobotService {

    @Autowired
    private EventListeningHandle eventListeningService;
    @Autowired
    private ConfigBean configBean;

    /**
     * 启动qq机器人
     *
     * @param qq       qq号
     * @param password qq号对应明文密码
     */
    public void start(String qq, String password) {
        final Bot bot = BotFactory.INSTANCE.newBot(Long.parseLong(qq), password, new BotConfiguration() {
            {
                // 加载设备信息
                this.loadDeviceInfoJson(DeviceUtil.getDeviceInfoJson(qq));
                // 使用安卓平板协议
                this.setProtocol(MiraiProtocol.ANDROID_PAD);
                // 工作空间目录，为根目录加登录的qq
                this.setCacheDir(new File(configBean.getWorkspace() + File.separator + "qq" + File.separator + qq));
                // 开启所有列表缓存
                // this.enableContactCache();
                // 自定义缓存
                ContactListCache cache = new ContactListCache();
                // 开启好友列表缓存
                cache.setFriendListCacheEnabled(true);
                // 开启群成员列表缓存
                cache.setGroupMemberListCacheEnabled(true);
                // 可选设置有更新时的保存时间间隔, 默认 60 秒
                cache.setSaveIntervalMillis(60000);
                this.setContactListCache(cache);
                if (!configBean.isLogOut()) {
                    // 关闭日志输出
                    this.noBotLog();
                    this.noNetworkLog();
                }
            }
        });
        try {
            // 注册QQ机器人事件监听
            bot.getEventChannel().registerListenerHost(eventListeningService);
            // 登录QQ
            bot.login();
            // 阻塞当前线程直到 bot 离线
            bot.join();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
