package com.billdowney.qq.robot.util;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.json.JSONObject;
import com.billdowney.qq.robot.bean.DeviceInfo;
import lombok.experimental.UtilityClass;

import java.io.File;

/**
 * 机器人终端设备工具类
 *
 * @author feitao yyimba@qq.com
 * @since 2020/11/4 6:11 下午
 */
@UtilityClass
public class DeviceUtil {

    /**
     * 获取机器人设备信息的JSON字符串
     *
     * @return
     */
    public String getDeviceInfoJson(String qq) {
        return new JSONObject(new DeviceInfo()).toString();
    }

    public String getDeviceInfoJson1(String qq) {
        // 设备信息文件
        File file = new File("deviceInfo-".concat(qq).concat(".json"));
        String deviceInfoJson = null;
        if (file.exists()) {
            FileReader fileReader = new FileReader(file);
            deviceInfoJson = fileReader.readString();
        } else {
            deviceInfoJson = new JSONObject().toString();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(deviceInfoJson);
        }
        return deviceInfoJson;
    }
}
