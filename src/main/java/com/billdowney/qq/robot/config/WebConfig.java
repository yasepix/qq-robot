package com.billdowney.qq.robot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author BillDowney
 * @date 2021/3/31 21:37
 */
@EnableAsync
@Configuration
public class WebConfig implements WebMvcConfigurer {


}
