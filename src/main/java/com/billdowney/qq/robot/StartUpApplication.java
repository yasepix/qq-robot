package com.billdowney.qq.robot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

/**
 * 启动类
 *
 * @author BillDowney
 */
@SpringBootApplication(scanBasePackages = {"cn.hutool.extra.spring", "com.billdowney.qq.robot"})
@Slf4j
public class StartUpApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(StartUpApplication.class);
    }

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext application = SpringApplication.run(StartUpApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        // WebServerApplicationContext context = application.getBean(WebServerApplicationContext.class);
        // int port = context.getWebServer().getPort();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        log.info("\n----------------------------------------------------------\n\t" +
                "Application is running! Access URLs:\n\t" +
                "Local:\t\thttp://127.0.0.1:" + port + path + "/\n\t" +
                "External:\thttp://" + ip + ":" + port + path + "/\n\t" +
                "Knife4j:\thttp://" + ip + ":" + port + path + "/doc.html\n\t" +
                "druid:\t\thttp://" + ip + ":" + port + path + "/druid\n" +
                "----------------------------------------------------------");
    }
}
