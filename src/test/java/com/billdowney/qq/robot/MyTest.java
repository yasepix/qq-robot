package com.billdowney.qq.robot;

import com.billdowney.qq.robot.event.EventListeningHandle;
import com.billdowney.qq.robot.util.DeviceUtil;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.BotFactory;
import net.mamoe.mirai.utils.BotConfiguration;
import org.junit.Test;

/**
 * @author BillDowney
 * @date 2021/3/30 20:23
 */
@Slf4j
public class MyTest {

    private final static String qq = "123456789";
    private final static String password = "123456789";

    @Test
    public void RobotTest() {
        final Bot bot = BotFactory.INSTANCE.newBot(Long.parseLong(qq), password, new BotConfiguration() {
            {
                /**
                 * 加载设备信息
                 */
                this.loadDeviceInfoJson(DeviceUtil.getDeviceInfoJson(qq));
                /**
                 * 使用安卓平板协议
                 */
                this.setProtocol(MiraiProtocol.ANDROID_PAD);
            }
        });
        try {
            // 注册QQ机器人事件监听
            bot.getEventChannel().registerListenerHost(new EventListeningHandle());
            // 登录QQ
            bot.login();
            // 阻塞当前线程直到 bot 离线
            bot.join();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
