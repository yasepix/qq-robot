# qq-robot

#### 介绍

- qq机器人，使用基础框架：https://github.com/mamoe/mirai
- 推荐一个spring boot启动脚本：https://gitee.com/billdowney/spring-boot-script

#### 软件架构

1. 采用插件式处理消息，可无限扩展插件，需要jdk1.8以上，maven3，暂时没有添加数据库相关操作，qq登录采用调用接口的方式，由于没有页面，所以直接采用knife4j的界面去调接口。  
   用户和群隔离开，每个群单独一个启动的配置，所有用户私聊一个启动配置。
2. 系统管理相关命令都在【系统管理插件】里，部分命令必须配置了root管理员才能使用，对应配置：`project.qq-robot.root-manage-qq`（例如开关群管理权限）
3. 扩展插件需要继承`RobotPlugin`才能被系统扫描到，并且搭配注解`@HookNotice`和`@HookMethod`使用
4. 命令开始字符可以通过配置修改：`project.qq-robot.cmd-char`，默认为`#`，可以自定义成其它字符，也可以是中文字咯~

#### 接口和注解

```java
/**
 * 机器人插件接口
 */
public interface RobotPlugin {
    /**
     * 在目标方法被调用之前做增强处理
     *
     * @param content 插件上下文
     */
    public default void before(RobotPluginContent content) {
    }

    /**
     * 在目标方法完成之后做增强，无论目标方法时候成功完成
     *
     * @param content 插件上下文
     */
    public default void after(RobotPluginContent content) {
    }
}

```

```java
/**
 * 消息通知钩子
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface HookNotice {
    /**
     * 名称
     */
    String name();

    /**
     * 描述
     */
    String[] desc() default {};

    /**
     * 默认是否启用
     */
    boolean start() default false;

    /**
     * 排序，数字越大越靠后，默认为10000
     */
    int order() default RobotPluginInfo.DEFAULT_ORDER;
}
```

```java
/**
 * 执行命令的方法注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface HookMethod {
    /**
     * 以字符开始的命令
     */
    String[] start() default {};

    /**
     * 完全匹配的命令
     */
    String[] equal() default {};

    /**
     * 普通管理员权限
     */
    boolean normalManager() default false;

    /**
     * root管理员权限
     */
    boolean rootManager() default false;

    /**
     * 适用的事件，不填默认只生效消息类型的事件，{@link RobotEventEnum#robotMessageEventEnums}
     */
    RobotEventEnum[] event() default {};

    /**
     * 描述
     */
    String desc() default "";
}
```

#### 安装教程

1. 下载源码，更新maven包
2. 启动类：`com.billdowney.qq.robot.StartUpApplication`
3. 默认文档地址：http://127.0.0.1:8282/qq-robot/doc.html
4. 由于其中用到了别人的api服务，需要修改`application.yml`中的配置：`project.qq-robot.alapi-token`、`project.qq-robot.kate-api-token`

#### 使用说明

1. 文档地址：机器人接口->启动机器人，输入登录的qq号和密码登录账号
2. 文档地址：机器人接口->机器人状态，可查看当前所有登录qq的状态，输入qq号查看指定的qq号状态
3. 文档地址：机器人接口->停止机器人，可直接退出所有的qq号，输入qq号退出指定的qq
4. 命令：#插件详情 系统管理插件，可查看当前所有管理员才能执行的命令，用于管理系统
5. 所有内置插件在`com.billdowney.qq.robot.plugins.provide`包里
6. 命令：#插件列表，可查看当前所有内置插件状态
7. 命令：#插件详情 {插件名称}，可查看对应插件简介以及使用方式
8. 命令：#刷新配置，可以直接加载`/cache/config.json`的配置文件，可以先修改文件内容，然后刷新到内存中

#### 工作空间目录说明

对应配置`project.qq-robot.workspace`的子目录结构说明

1. group_manage，群管理插件文件夹
    1. leave_group，离群人员名单存放文件夹
        1. {机器人qq}/{群qq}
    2. welcome_img，欢迎图文件夹
2. media，媒体插件文件夹
    1. img，看图
    2. short_video，短视频
    3. sticker，斗图
3. qq，mirai登录qq缓存文件夹
    1. ${机器人qq}
4. config.json，第一次启动后的初始化的配置文件

#### 已知问题

1. [常见问题](https://docs.mirai.mamoe.net/Bots.html#%E5%B8%B8%E8%A7%81%E7%99%BB%E5%BD%95%E5%A4%B1%E8%B4%A5%E5%8E%9F%E5%9B%A0)
2. 假如出现能发送好友消息，发送不了群消息，可以尝试删除`cache/qq/[登录的qq号]`文件夹，怀疑是缓存的服务器列表发送不了
3. 软件第一次启动后会把所有的`application.yml`中`project.qq.robot`配置全部写入到`config.json`[工作空间目录说明](#工作空间目录说明)
   ，所以当配置不生效就需要修改该文件，并且使用命令`刷新配置`重新载入

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
